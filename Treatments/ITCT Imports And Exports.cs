﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;

namespace Treatments
{
    public class InTransitTreatments
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="usda1"></param>
        /// <param name="usda2"></param>
        /// <param name="usda3"></param>
        /// <returns>Maximum and minimum temperature out of three probes</returns>
        public static (double maxTemp, double minTemp) MinAndMaxProbeTemperatures(double usda1, double usda2, double usda3)
        {
            double tempProbeOne;
            double tempProbeTwo;
            double tempProbeThree;
            List<double> tempList = new List<double>();
            double maxVal;
            double minVal;
            try
            {
                tempProbeOne = (usda1);
                tempProbeTwo = (usda2);
                tempProbeThree = (usda3);
                tempList.Add(tempProbeOne);
                tempList.Add(tempProbeTwo);
                tempList.Add(tempProbeThree);
                maxVal = tempList.Max();
                minVal = tempList.Min();

            }
            catch (Exception)
            {
                maxVal = 9999;
                minVal = -9999;
            }
            return (maxVal, minVal);
        }

        /// <summary>
        /// Read the text file and find the indexes of date, time,USDA1,USDA2,USDA3 and the line number where it starts. As, there can be unwanted text at the 
        /// begining of the file.
        /// </summary>
        /// <param name="path">Text file path</param>
        /// <returns>An Array with the indexes of date,time and three probe readings</returns>
        public static int[] GetFirstLine(string path)
        {
            Logger.AddLine($"Line:{Logger.LineNumber()}|| [GetFirstLine function been instantiated]");
            int lineWithDateIndex;
            int usdaIndex = 0;
            int dateIndex = 0;
            int[] intArr = new int[3] { 0, 0, 0 };
            //List<int> allIndexes = new List<int>();
            string[] line;
            int firstLine = 0;
            int flag = 1;
            try
            {
                line = File.ReadAllLines(path);
                for (int i = 0; i < line.Length; i++)
                {
                    if ((line[i].ToLower().Contains("usda1") || line[i].Contains("USDA#1(C)")) && flag == 1 )
                    {
                        Logger.AddLine($"Line:{Logger.LineNumber()}|| Column names 'USDA' OR 'USDA#' found at this line of the text file :" + i);
                        firstLine = i;
                        flag = 0;
                    }
                }
            }
            catch (Exception e)
            {
                Logger.AddLine($"Line:{Logger.LineNumber()}||" + e);
                line = null;
            }
            //Index where Date is found in the text file
            lineWithDateIndex = firstLine;

            var values = line[firstLine].Split(new char[] { ' ' }, StringSplitOptions.RemoveEmptyEntries);
            for (int i = 0; i < values.Length; i++)
            {
                if (values[i].ToLower() == "date")
                {
                    dateIndex = i;
                    Logger.AddLine($"Line:{Logger.LineNumber()}|| Date Index found at: " + i);
                }
                if ((values[i].ToLower() == "usda1") || (values[i] == "USDA#1(C)"))
                {
                    usdaIndex = i;
                    Logger.AddLine($"Line:{Logger.LineNumber()}|| USDA1 OR USDA#1 Index Found at: " + i);
                }
            }
            if (usdaIndex == 0)
            {
                Logger.AddLine($"Line:{Logger.LineNumber()}|| [USDA1 OR USDA#1 Index Not Found]");
            }

            //Adding 1 to start to skip over headers
            intArr[0] = lineWithDateIndex + 1;
            intArr[1] = usdaIndex;
            intArr[2] = dateIndex;
            Logger.AddLine($"Line:{Logger.LineNumber()}|| [Array returned from getfirstline method: {0} {1} {2}]" + intArr[0].ToString() + "," + intArr[1].ToString() + "," + intArr[2].ToString());
            return intArr;
        }

        /// <summary>
        /// Returns the difference of two different dateTimes
        /// </summary>
        /// <param name="startDate"></param>
        /// <param name="startTime"></param>
        /// <param name="endDate"></param>
        /// <param name="endTime"></param>
        /// <returns>difference of two dates provided</returns>
        public static double DateLogic(string startDate, string startTime, string endDate, string endTime)
        {
            double daysDifference;
            try
            {
                DateTime firstDateAndTimeOccurred = DateTime.Parse(startDate.Replace("'", "20") + " " + startTime);
                DateTime lastDateAndTimeOccured = DateTime.Parse(endDate.Replace("'", "20") + " " + endTime);
                TimeSpan timeDifference = lastDateAndTimeOccured.Subtract(firstDateAndTimeOccurred);
                daysDifference = timeDifference.TotalDays;
            }
            catch
            {
                daysDifference = -999;
            }
            return daysDifference;
        }
        /// <summary>
        /// Save data into a multi dimensional arrray
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="jaggedArray"></param>
        /// <returns></returns>
        public static string[,] JaggedToMultidimensional<T>(T[][] jaggedArray)
        {
            int rows = jaggedArray.Length;
            int cols = jaggedArray.Max(subArray => subArray.Length);
            string[,] array = new string[rows, cols];
            var regex = new Regex(@"^\s*$");

            for (int i = 0; i < rows; i++)
            {
                cols = jaggedArray[i].Length;
                for (int j = 0; j < cols; j++)
                {
                    if (String.IsNullOrWhiteSpace(jaggedArray[i][j].ToString()))
                    {
                        array[i, j] = "OOR";
                    }
                    else
                    {
                        array[i, j] = jaggedArray[i][j].ToString();
                    }

                }

            }
            Logger.AddLine($"Line:{Logger.LineNumber()}|| Returned a multi dimentional array of length: " + rows + " and columns: " + cols);
            return array;
        }

        public static bool FourConsecutiveBlankCheck(String[,] jaggedArray, string startDate, string startTime, int dateIndex, int timeIndex, int probeOneIndex, int probeTwoIndex, int probeThreeIndex, string treatmentTotalDays)
        {
            List<int> blankIndexes = new List<int>();
            bool fourConsecutiveBlankCheck = false;
            int lastIndex = 0;
            double missingRows = 0;
            var csv = new StringBuilder();
            for (int i = 0; i < jaggedArray.GetUpperBound(0); i++)
            {
                if (DateLogic(startDate, startTime, jaggedArray[i, dateIndex], jaggedArray[i, timeIndex]) >= 0)
                {
                    for (int j = i; j < jaggedArray.GetUpperBound(0) + 1; j++)
                    {
                        if (DateLogic(jaggedArray[i, dateIndex], jaggedArray[i, timeIndex], jaggedArray[j, dateIndex], jaggedArray[j, timeIndex]) >= double.Parse(treatmentTotalDays))
                        {
                            lastIndex = j;
                            break;
                        }
                    }
                    break;
                }
            }

            for (int i = 0; i < lastIndex; i++)
            {
                if (DateLogic(startDate, startTime, jaggedArray[i, dateIndex], jaggedArray[i, timeIndex]) >= 0)
                {
                    double maxTemp, minTemp;
                    try
                    {
                        (maxTemp, minTemp) = MinAndMaxProbeTemperatures(Double.Parse(jaggedArray[i, probeOneIndex]), Double.Parse(jaggedArray[i, probeTwoIndex]), Double.Parse(jaggedArray[i, probeThreeIndex]));
                    }
                    catch
                    {
                        maxTemp = 999;
                        minTemp = -999;
                    }
                    if (DateLogic(jaggedArray[i, dateIndex], jaggedArray[i, timeIndex], jaggedArray[i + 1, dateIndex], jaggedArray[i + 1, timeIndex]) > 0.041666666666666664)
                    {
                        missingRows = DateLogic(jaggedArray[i, dateIndex], jaggedArray[i, timeIndex], jaggedArray[i + 1, dateIndex], jaggedArray[i + 1, timeIndex]) / 0.041666666666666664;
                        if (missingRows > 3)
                        {
                            break;
                        }
                    }

                }
            }

            if (missingRows > 3)
            {
                fourConsecutiveBlankCheck = true;
            }
            else
            {
                fourConsecutiveBlankCheck = false;
            }

            return fourConsecutiveBlankCheck;
        }

        /// <summary>
        /// After finding a match of start and end date with the temperature requirements, this method checks all the lines within these datetimes and returns true
        /// if they meet the temperature requirements.
        /// </summary>
        /// <param name="startIndex"></param>
        /// <param name="endIndex"></param>
        /// <param name="temperatureToCompare"></param>
        /// <param name="dataSet"></param>
        /// <param name="usda1"></param>
        /// <param name="usda2"></param>
        /// <param name="usda3"></param>
        /// <param name="cal1"></param>
        /// <param name="cal2"></param>
        /// <param name="cal3"></param>
        /// <param name="tempLimit"></param>
        /// <returns></returns>
        public static bool ImportsCheckTemperaturesAfterMatch(int startIndex, int endIndex, string temperatureToCompare, String[,] dataSet, int usda1, int usda2, int usda3, string cal1, string cal2, string cal3, string tempLimit)
        {
            Logger.AddLine($"Line:{Logger.LineNumber()}|| checking probe temperatures of all lines from the start index " + startIndex + " and end index at " + endIndex);
            bool temperatureCheckAfterMatch = false;
            var regex = new Regex(@"^-?[0-9][0-9,\.]*$");
            for (int i = startIndex; i <= endIndex; i++)
            {
                double maxTemperature;
                double minTemperature;
                try
                {
                    (maxTemperature, minTemperature) = InTransitTreatments.MinAndMaxProbeTemperatures(Double.Parse(dataSet[i, usda1]) - Double.Parse(cal1),
                                                                                                      Double.Parse(dataSet[i, usda2]) - Double.Parse(cal2),
                                                                                                      Double.Parse(dataSet[i, usda3]) - Double.Parse(cal3));
                    if (minTemperature >= Double.Parse(tempLimit))
                    {
                        if (maxTemperature <= double.Parse(temperatureToCompare))
                        {
                            temperatureCheckAfterMatch = true;
                        }
                        else
                        {
                            temperatureCheckAfterMatch = false;
                            Logger.AddLine($"Line:{Logger.LineNumber()}|| Probe temperature Check FAILED at line index" + i +
                                "where probe1=" + dataSet[i, usda1] + "probe2=" + dataSet[i, usda2] + "probe3" + dataSet[i, usda3] + "Comparing against the temperature " + maxTemperature);
                            break;
                        }
                    }
                    else
                    {
                        temperatureCheckAfterMatch = false;
                        Logger.AddLine($"Line:{Logger.LineNumber()}|| FAIL(greater than" + tempLimit + ") Found Temperature of a probe below " + tempLimit + " degrees, Probe temperature Check FAILED at line index" + i +
                                "where probe1=" + dataSet[i, usda1] + "probe2=" + dataSet[i, usda2] + "probe3" + dataSet[i, usda3]);
                        break;
                    }
                }
                catch (Exception e)
                {
                    temperatureCheckAfterMatch = false;
                    Logger.AddLine($"Line:{Logger.LineNumber()}|| " + e + "\n" + " on this date " + dataSet[i, 0] + " at " + dataSet[i, 1]);
                    break;
                }
            }
            if (temperatureCheckAfterMatch)
            {
                Logger.AddLine($"Line:{Logger.LineNumber()}|| PASS PASS PASS!! All the three probe temperatures are less than or equal to the provided temp");
            }
            return temperatureCheckAfterMatch;
        }

        /// <summary>
        /// This function returns the indexes of date, time and three probes along with the array(text from the provided text file)
        /// </summary>
        /// <param name="textFilePath"></param>
        /// <returns></returns>
        public static (string[,] jaggedArray, int probeOneIndex, int probeTwoIndex, int probeThreeIndex, int dateIndex, int timeIndex) ArrayFilter(string textFilePath)
        {
            var requiredIndexes = InTransitTreatments.GetFirstLine(textFilePath);
            var regex = new Regex(@"^-?[0-9][0-9,\.]*$");
            int startLineIndex = requiredIndexes[0];
            int probeOne = requiredIndexes[1];
            int probeTwo = requiredIndexes[1] + 1;
            int probeThree = requiredIndexes[1] + 2;
            int dateIndex = requiredIndexes[2];
            int timeIndex = requiredIndexes[2] + 1;

            //structuring all data into a 2d array, amkes it easy to perform treatment checks
            //Using start index to skip over the header so that the 2d array only has data
            string[][] filteredArray = File.ReadAllLines(textFilePath).Skip(startLineIndex)
                .Where(l => l != "" && !l.Contains("CONTAINER") && !l.Contains("USDA#1(C)") && !l.Contains("END"))
                .Select(l => l.Split(new char[] { ' ', '\t' }, StringSplitOptions.RemoveEmptyEntries)).ToArray();

            string[,] fileData = JaggedToMultidimensional(filteredArray);

            return (fileData, probeOne, probeTwo, probeThree, dateIndex, timeIndex);
        }
        /// <summary>
        /// /// <summary>
        /// Performs core logic
        /// Reads the array generated from the text file
        /// ignores the probe readings containing OOR(Out of Range)
        /// gets the max value out of three probe readings
        /// compares to the minimum temperature requirements from the config file
        /// gets the index of the first match
        /// using the number of days(the temperature to be maintained within the range), multiply it with 24(no. of hours) to look for a exact match
        /// if match is not found, index moves to the next line 
        /// when a match is found, gets the start and end index.
        /// checkTemperaturesAfterMatch method checks all the max temperatures individually within the range
        /// </summary>
        /// <param name="textFilePath"></param>
        /// <param name="temperatureToCompare"></param>
        /// <param name="daysOfTreatment"></param>
        /// <param name="cal1"></param>
        /// <param name="cal2"></param>
        /// <param name="cal3"></param>
        /// <param name="temperatureLimit"></param>
        /// <returns></returns>

        public static string[] ImportsCoreLogic(string textFilePath, string temperatureToCompare, string daysOfTreatment, string cal1, string cal2, string cal3, string temperatureLimit)
        {
            //Replace USDA 1,USDA 2 and USDA 3 to USDA1,USDA2 and USDA3. As these are the column headers, spaces in between the headers in a text file creates issues.
            string findAndReplace = File.ReadAllText(textFilePath);
            findAndReplace = findAndReplace.Replace("USDA 1", "USDA1").Replace("USDA 2", "USDA2").Replace("USDA 3", "USDA3");
            File.WriteAllText(textFilePath, findAndReplace);
            var (jaggedArray, probeOneIndex, probeTwoIndex, probeThreeIndex, dateIndex, timeIndex) = ArrayFilter(textFilePath);

            Logger.AddLine($"Line:{Logger.LineNumber()}|| textfile path" + textFilePath + " sent to ArrayFilter Function");
            Logger.AddLine($"Line:{Logger.LineNumber()}|| Received array of length" + jaggedArray.Length + ",probe indexes probe1:"
                + probeOneIndex.ToString() + ",probe2:" + probeTwoIndex.ToString() + ",probe3:" + probeThreeIndex.ToString() +
                ",date Index:" + dateIndex + "and time Index:" + timeIndex + " from the array generated from the dataset within text file");

            string[] oneDArray = new string[10];
            var regex = new Regex(@"^-?[0-9][0-9,\.]*$");
            for (int i = 0; i < jaggedArray.GetUpperBound(0) + 1; i++)
            {
                if (!String.IsNullOrWhiteSpace(jaggedArray[i, probeOneIndex]) &&
                    !String.IsNullOrWhiteSpace(jaggedArray[i, probeTwoIndex]) &&
                    !String.IsNullOrWhiteSpace(jaggedArray[i, probeThreeIndex]) &&
                    (jaggedArray[i, 1].Contains(":") || jaggedArray[i, 1].Contains("/")) &&
                    regex.IsMatch(jaggedArray[i, probeOneIndex]) && regex.IsMatch(jaggedArray[i, probeTwoIndex]) && regex.IsMatch(jaggedArray[i, probeThreeIndex]))
                {
                    double maxStartTemp, minStartTemp;
                    try
                    {
                        (maxStartTemp, minStartTemp) = MinAndMaxProbeTemperatures((Double.Parse(jaggedArray[i, probeOneIndex]) - Double.Parse(cal1)),
                         (Double.Parse(jaggedArray[i, probeTwoIndex]) - Double.Parse(cal2)),
                         (Double.Parse(jaggedArray[i, probeThreeIndex]) - Double.Parse(cal3)));
                    }
                    catch
                    {
                        maxStartTemp = 9999;
                    }
                    
                    //check probe readings are in correct format before finding Maximium temp out of three probes
                    //check if the maxTemp of three probes is less than or equal to the tempeature to compare against
                    if ((maxStartTemp <= double.Parse(temperatureToCompare)) && (maxStartTemp != 9999))
                    {
                        string firstDateOccurred = jaggedArray[i, dateIndex];
                        string firstTimeOccured = jaggedArray[i, timeIndex];
                        //find the first line within the temp requirement and find the end line by multiplying the days of trstment with 24(Hours in a day)
                        //exaample: first line-2019/10/01 9:00, No. of days to check:7days,so the end index will be 7*24=168, 168 rows to check to find a match.
                        bool fourBlankRowsCheck = FourConsecutiveBlankCheck(jaggedArray,firstDateOccurred,firstTimeOccured,dateIndex,timeIndex,probeOneIndex,probeTwoIndex,probeThreeIndex,daysOfTreatment);
                        int numberOfRowsToMatch = (int)Math.Round(double.Parse(daysOfTreatment) * 24);
                        //check the last index probe temperatures
                        if ((i + numberOfRowsToMatch) <= jaggedArray.GetUpperBound(0) && jaggedArray[i, probeOneIndex] != null && !fourBlankRowsCheck)
                        {
                            int endIndex = i + numberOfRowsToMatch;
                            string lastDateOccurred = jaggedArray[endIndex, dateIndex];
                            string lastTimeOccured = jaggedArray[endIndex, timeIndex];
                            int daysDifferernce = (int) Math.Round(DateLogic(firstDateOccurred, firstTimeOccured, lastDateOccurred, lastTimeOccured));
                            double maxEndTemp, minEndTemp;
                            try
                            {
                                (maxEndTemp, minEndTemp) = MinAndMaxProbeTemperatures((Double.Parse(jaggedArray[endIndex, probeOneIndex]) - Double.Parse(cal1)),
                                    (Double.Parse(jaggedArray[endIndex, probeTwoIndex]) - Double.Parse(cal2)),
                                    (Double.Parse(jaggedArray[endIndex, probeThreeIndex]) - Double.Parse(cal3)));
                            }
                            catch (Exception e)
                            {
                                maxEndTemp = 9999;
                                Logger.AddLine($"Line:{Logger.LineNumber()}|| " + e + "\n" + " on this date " + jaggedArray[endIndex, 0] + " at " + jaggedArray[endIndex, 1]);
                            }
                            if ((daysDifferernce == Math.Round(double.Parse(daysOfTreatment))) && (maxEndTemp <= double.Parse(temperatureToCompare)))
                            {
                                Logger.AddLine($"Line:{Logger.LineNumber()}|| Found a start date" + firstDateOccurred + firstTimeOccured +
                                    " and end date " + lastDateOccurred + lastTimeOccured);
                                Logger.AddLine($"Line:{Logger.LineNumber()}|| checkTemperaturesAfterMatch Function is called");
                                bool check = ImportsCheckTemperaturesAfterMatch(i, endIndex, temperatureToCompare, jaggedArray, probeOneIndex, probeTwoIndex, probeThreeIndex, cal1, cal2, cal3, temperatureLimit);
                                if (check)
                                {
                                    Logger.AddLine($"Line:{Logger.LineNumber()}|| checkTemperaturesAfterMatch Function returned SUCCESS");
                                    oneDArray[0] = "MATCH";
                                    oneDArray[1] = (jaggedArray[i, dateIndex].ToString() + " " + jaggedArray[i, timeIndex].ToString() + "," + i);
                                    oneDArray[2] = (jaggedArray[endIndex, dateIndex].ToString() + " " + jaggedArray[endIndex, timeIndex].ToString() + "," + endIndex);

                                    break;
                                }
                                else
                                {
                                    oneDArray[0] = "FAIL";
                                    Logger.AddLine($"Line:{Logger.LineNumber()}|| checkTemperaturesAfterMatch Function returned FAIL");
                                }
                            }
                        }
                    }
                }
            }
            Console.WriteLine(oneDArray[0]);
            Console.WriteLine(oneDArray[1]);
            Console.WriteLine(oneDArray[2]);
            return oneDArray;
        }

        //-------------------------------------
        //USAImports
        //-------------------------------------

        public static string[] ImportsCoreLogicUSA(string textFilePath, string temperatureToCompare, string daysOfTreatment, string cal1, string cal2, string cal3, string temperatureLimit,string firstMatchMinimumProbeTemp)
        {
            //Replace USDA 1,USDA 2 and USDA 3 to USDA1,USDA2 and USDA3. As these are the column headers, spaces in between the headers in a text file creates issues.
            string findAndReplace = File.ReadAllText(textFilePath);
            findAndReplace = findAndReplace.Replace("USDA 1", "USDA1").Replace("USDA 2", "USDA2").Replace("USDA 3", "USDA3");
            File.WriteAllText(textFilePath, findAndReplace);
            var (jaggedArray, probeOneIndex, probeTwoIndex, probeThreeIndex, dateIndex, timeIndex) = ArrayFilter(textFilePath);

            Logger.AddLine($"Line:{Logger.LineNumber()}|| textfile path" + textFilePath + " sent to ArrayFilter Function");
            Logger.AddLine($"Line:{Logger.LineNumber()}|| Received array of length" + jaggedArray.Length + ",probe indexes probe1:"
                + probeOneIndex.ToString() + ",probe2:" + probeTwoIndex.ToString() + ",probe3:" + probeThreeIndex.ToString() +
                ",date Index:" + dateIndex + "and time Index:" + timeIndex + " from the array generated from the dataset within text file");

            string[] oneDArray = new string[10];
            var regex = new Regex(@"^-?[0-9][0-9,\.]*$");
            for (int i = 0; i < jaggedArray.GetUpperBound(0) + 1; i++)
            {
                if (!String.IsNullOrWhiteSpace(jaggedArray[i, probeOneIndex]) &&
                    !String.IsNullOrWhiteSpace(jaggedArray[i, probeTwoIndex]) &&
                    !String.IsNullOrWhiteSpace(jaggedArray[i, probeThreeIndex]) &&
                    (jaggedArray[i, 1].Contains(":") || jaggedArray[i, 1].Contains("/")) &&
                    regex.IsMatch(jaggedArray[i, probeOneIndex]) && regex.IsMatch(jaggedArray[i, probeTwoIndex]) && regex.IsMatch(jaggedArray[i, probeThreeIndex]))
                {
                    double maxStartTemp, minStartTemp;
                    try
                    {
                        (maxStartTemp, minStartTemp) = MinAndMaxProbeTemperatures((Double.Parse(jaggedArray[i, probeOneIndex]) - Double.Parse(cal1)),
                         (Double.Parse(jaggedArray[i, probeTwoIndex]) - Double.Parse(cal2)),
                         (Double.Parse(jaggedArray[i, probeThreeIndex]) - Double.Parse(cal3)));
                    }
                    catch
                    {
                        maxStartTemp = 9999;
                    }

                    //check probe readings are in correct format before finding Maximium temp out of three probes
                    //check if the maxTemp of three probes is less than or equal to the tempeature to compare against
                    if ((maxStartTemp <= double.Parse(firstMatchMinimumProbeTemp)) && (maxStartTemp != 9999))
                    {
                        string firstDateOccurred = jaggedArray[i, dateIndex];
                        string firstTimeOccured = jaggedArray[i, timeIndex];
                        //find the first line within the temp requirement and find the end line by multiplying the days of trstment with 24(Hours in a day)
                        //exaample: first line-2019/10/01 9:00, No. of days to check:7days,so the end index will be 7*24=168, 168 rows to check to find a match.
                        bool fourBlankRowsCheck = FourConsecutiveBlankCheck(jaggedArray, firstDateOccurred, firstTimeOccured, dateIndex, timeIndex, probeOneIndex, probeTwoIndex, probeThreeIndex, daysOfTreatment);
                        int numberOfRowsToMatch = (int)Math.Round(double.Parse(daysOfTreatment) * 24);
                        //check the last index probe temperatures
                        if ((i + numberOfRowsToMatch) <= jaggedArray.GetUpperBound(0) && jaggedArray[i, probeOneIndex] != null && !fourBlankRowsCheck)
                        {
                            int endIndex = i + numberOfRowsToMatch;
                            string lastDateOccurred = jaggedArray[endIndex, dateIndex];
                            string lastTimeOccured = jaggedArray[endIndex, timeIndex];
                            int daysDifferernce = (int)Math.Round(DateLogic(firstDateOccurred, firstTimeOccured, lastDateOccurred, lastTimeOccured));
                            double maxEndTemp, minEndTemp;
                            try
                            {
                                (maxEndTemp, minEndTemp) = MinAndMaxProbeTemperatures((Double.Parse(jaggedArray[endIndex, probeOneIndex]) - Double.Parse(cal1)),
                                    (Double.Parse(jaggedArray[endIndex, probeTwoIndex]) - Double.Parse(cal2)),
                                    (Double.Parse(jaggedArray[endIndex, probeThreeIndex]) - Double.Parse(cal3)));
                            }
                            catch (Exception e)
                            {
                                maxEndTemp = 9999;
                                Logger.AddLine($"Line:{Logger.LineNumber()}|| " + e + "\n" + " on this date " + jaggedArray[endIndex, 0] + " at " + jaggedArray[endIndex, 1]);
                            }
                            if ((daysDifferernce == Math.Round(double.Parse(daysOfTreatment))) && (maxEndTemp <= double.Parse(temperatureToCompare)))
                            {
                                Logger.AddLine($"Line:{Logger.LineNumber()}|| Found a start date" + firstDateOccurred + firstTimeOccured +
                                    " and end date " + lastDateOccurred + lastTimeOccured);
                                Logger.AddLine($"Line:{Logger.LineNumber()}|| checkTemperaturesAfterMatch Function is called");
                                bool check = ImportsCheckTemperaturesAfterMatch(i, endIndex, temperatureToCompare, jaggedArray, probeOneIndex, probeTwoIndex, probeThreeIndex, cal1, cal2, cal3, temperatureLimit);
                                if (check)
                                {
                                    Logger.AddLine($"Line:{Logger.LineNumber()}|| checkTemperaturesAfterMatch Function returned SUCCESS");
                                    oneDArray[0] = "MATCH";
                                    oneDArray[1] = (jaggedArray[i, dateIndex].ToString() + " " + jaggedArray[i, timeIndex].ToString() + "," + i);
                                    oneDArray[2] = (jaggedArray[endIndex, dateIndex].ToString() + " " + jaggedArray[endIndex, timeIndex].ToString() + "," + endIndex);

                                    break;
                                }
                                else
                                {
                                    oneDArray[0] = "FAIL";
                                    Logger.AddLine($"Line:{Logger.LineNumber()}|| checkTemperaturesAfterMatch Function returned FAIL");
                                }
                            }
                        }
                    }
                }
            }
            Console.WriteLine(oneDArray[0]);
            Console.WriteLine(oneDArray[1]);
            Console.WriteLine(oneDArray[2]);
            return oneDArray;
        }
    }
}

