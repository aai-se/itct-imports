﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

namespace Treatments
{
    public static class Logger
    {
        //For logging to work the logger class needs to have data in the string filePath variable, if the filePath variable is "" then the logger 
        //will not create the file and folders and will not add logging info to the txt.
        /// <summary>
        /// To Use Logger:
        /// 1st -> Create the folder path using the CreateFolder Method
        /// 2nd -> in your code use AddLine Method and provide the messasge you would like to log
        /// 3rd -> There is no Third :)
        /// NOTE: Best way to log things is to use:
        ///"Logger.AddLine($"Line:{Logger.LineNumber()}|| [YOUR_MESSAGE]");"
        /// </summary>

        //public static string filePath = $"C:\\ConsignmentLinkingLogs\\{DateTime.Today.ToString("yymmdd")}\\";
        public static string filePath = $"";
        public static string filename = "Log.txt";
        // Write the string to a file.append mode is enabled so that the log
        // lines get appended to  test.txt than wiping content and writing the log

        public static void AddLine(string line)
        {
            //if filepath is not nothing then we add line to txt file
            if (filePath != "")
            {
                StreamWriter file = new System.IO.StreamWriter(filePath + filename, true);
                file.WriteLine(line + "\n");
                file.Close();
            }

        }
        public static int LineNumber([System.Runtime.CompilerServices.CallerLineNumber] int lineNumber = 0)
        {
            return lineNumber;
        }
        public static void CreateFolder(string @loggingFolderPath)
        {
            //if parameter is not nothing then we create the logging file and folder structure
            if (loggingFolderPath != "")
            {
                if (loggingFolderPath.Contains(".txt"))
                {
                    loggingFolderPath.Replace(".txt", "");
                }
                filename = $"{loggingFolderPath.Split('\\').Last()}{DateTime.Now.ToString("yyyymmdd_hhmm")}.txt";
                filePath = string.Join("\\", loggingFolderPath.Split('\\').Take(loggingFolderPath.Split('\\').Length - 1).ToArray()) + "\\";
                Directory.CreateDirectory(filePath);
                // filename = $"Log{DateTime.Now.ToString("hh:mm:ss").Replace(":", "_")}.txt";
            }
        }

    }

}
