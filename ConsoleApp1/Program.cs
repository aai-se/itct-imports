﻿
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Text;
using Treatments;

namespace ConsoleApp1
{
    class Program
    {
        public static void Main(string[] args)
        {
            String textFilepath = @"C:\InTransit\TTNU8256661.TXT";
            Console.WriteLine(Treatments.InTransitTreatments.DateLogic("2020/09/ 13", "05:00", "2020 / 09 / 13", "15:00"));
            //String textFilepath1 = @"C:\InTransit\testFiles\end to end test\TTNU8287615.txt";
            Treatments.InTransitTreatments.ImportsCoreLogic(textFilepath, "1.67", "12","0.0","-0.2","-0.1","-3");
            Treatments.InTransitTreatments.ImportsCoreLogicUSA(textFilepath, "1.4", "12", "0.0", "0.0", "0.0", "-3","0.9");
        }
    }

}
