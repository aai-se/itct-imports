IMPORTS-ITCT METABOT
Metabot to perform treatment assessment on a given container using the information extracted from the provided text file, treatment schedule, number of treatment days and three calibration values.


Usage: ImportsCoreLogic($textFilePath$, $temperatureToCompare$, $daysOfTreatment$, $calibration1$, $calibration2$, $calibration3$, $temperatureLimit$)
Usage: ImportsCoreLogicUSA($textFilePath$, $temperatureToCompare$, $daysOfTreatment$, $calibration1$, $calibration2$, $calibration3$, $temperatureLimit$, $firstMatchMinimumProbeTemp$)

->textFilePath: Absolute path of the text file extracted from the shiping software.
->temperatureToCompare: upper limit of the temperature, this changes accordingly with the commodity type and source of country.
->daysOfTreatment: container temperature to be maintained for n consecutive number of days.
->calibration: Temperature values to be adjusted.
->temperatureLimit: Lower limit of the temperature.